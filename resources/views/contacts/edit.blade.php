@extends('layouts.app')

@push('style')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Contacts App
                </div>

                <div class="card-body">
                    <form action="/contact/{{ $contact->id }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="string" name="name" class="form-control" value="{{ $contact->name }}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" name="email" class="form-control" value="{{ $contact->email }}">
                    </div>
                    
                    <div class="form-group">
                        <label for="exampleInputPassword1">Phone</label>
                        <input type="string" name="phone" class="form-control" value="{{ $contact->phone }}">
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Update</button>  
                    <a href="/contact" class="btn btn-secondary ml-1">Cancel</a>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
