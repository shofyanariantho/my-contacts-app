@extends('layouts.app')

@push('style')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between">Contacts App
                </div>

                <div class="card-body table-responsive">
                    <div class="d-flex justify-content-between mb-3">
                        <a href={{ '/contact/create' }} class="btn btn-primary btn-md"> 
                            Add
                        </a>
                        
                        <form class="form-inline">
                            <div class="input-group">
                                <input type="text" name="search" value="{{ request()->search }}" class="form-control" placeholder="Search...">
                                <button class="btn btn-secondary btn-sm">Go !</button>
                            </div>
                        </form>
                    </div>
                    


                    <table class="table">
                    <thead class="thead-light">
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        @forelse ($contacts as $key => $item)
                        <tr>
                            <td class="align-middle">{{$contacts->firstItem() + $key}}</td>
                            <td class="align-middle">{{$item->name}}</td>
                            <td class="align-middle">{{$item->email}}</td>
                            <td class="align-middle">{{$item->phone}}</td>
                            <td class="align-middle"> 
                                <form action="/contact/{{$item->id}}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <a href="/contact/{{$item->id}}" class="btn btn-info btn-sm mb-1">Detail</a>
                                    @auth
                                    <a href="/contact/{{$item->id}}/edit" class="btn btn-warning btn-sm mb-1">Edit</a>
                                    <input type="submit" class="btn btn-danger btn-sm mb-1" value="Delete">
                                    @endauth
                                </form>    
                            </td>
                        
                        @empty
                            <div class="alert alert-danger alert-link">Not Found!</div>
                        @endforelse
                        </tr>
                    </tbody>
                </table>    
                {{ $contacts->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
