@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Welcome') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    @guest
                        {{ __("Welcome in Contacts App. You can manage your contact freely!") }}
                    @else
                        {{ __('You are logged in!') }}
                        <a href="/contact">Go to Contacts Manager</a>
                    @endguest
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
