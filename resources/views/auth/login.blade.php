@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">                    
                    <div class="mt-0 d-flex justify-content-center">
                        <div class="">
                            <a href="{{ route('google.login') }}">
                                <img src="https://developers.google.com/identity/images/btn_google_signin_dark_normal_web.png" >
                            </a> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
