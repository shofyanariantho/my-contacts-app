<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Create Data View
    public function create() {
        return view('contacts.input');
    }

    // Store Data
    public function store(Request $request) {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ], [
            'name.required' => 'Name is required!',
            'email.required' => 'Email is required!',
            'phone.required' => 'Phone is required!',
        ]);

        $contact = new Contact;
      
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->phone = $request->phone;
        $contact->user_id = Auth::id();
        
        $contact->save();

        return redirect('/contact')->with('success','Contact created successfully.');
    }

    // Index of Data
    public function index() {
        
        $contacts = Contact::latest()->filter(request(['search']))->paginate(5);

        return view('contacts.index',compact('contacts'));
    }

    // Show Data
    public function show(Contact $contact) {
        return view('contacts.show',compact('contact'));
    }

    // Update Data
    public function edit(Contact $contact) {
        return view('contacts.edit',compact('contact'));
    }

    public function update(Request $request, Contact $contact) {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ]);
      
        $contact->update($request->all());
      
        return redirect('/contact'.'/'.$contact->id)->with('success','Contact updated successfully');
    }

    // Destroy
    public function destroy(Contact $contact) {
        $contact->delete();
       
        return redirect('/contact')->with('success','Contact deleted successfully');
    }
}
