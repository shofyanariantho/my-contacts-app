<?php

namespace App\Http\Controllers\Auth;

use Exception;


use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Google_Client;
use Google_Service_People;


class GoogleController extends Controller
{
    public function googleRedirect() {
        return Socialite::driver('google')->redirect();
    }

    public function googleCallback() {
        try {
            $googleUser = Socialite::driver('google')->stateless()->user();
       
            $user = DB::table('users')->where('social_id', $googleUser->id)->first();

            if ($user){
                Auth::loginUsingId($user->id);
                return redirect('/home');
            } else{                
                return redirect('/register');
            }

        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }

    

}
