<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Contact extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeFilter($query, array $filters) 
    {   
        $query->when($filters['search'] ?? false, function($query, $search){
            return $query->where('name', 'like', "%{$search}%")
                ->orWhere('email', 'like', "%{$search}%" )
                ->orWhere('phone', 'like', "%{$search}%" );
        });
    }
    
}
